import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import DynamicComponentTest from './views/dynamic_component_test/Parent.vue'
import SyncTest from './views/sync_test/Parent.vue'
import WitoutVuexParent from './views/without_vuex/Parent.vue'
import WithVuexInput1 from './views/with_vuex/Input1.vue'
import WithVuexInput2 from './views/with_vuex/Input2.vue'
import WithVuexConfirm from './views/with_vuex/Confirm.vue'
import WithVuexComplete from './views/with_vuex/Complete.vue'

import store from './store'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/dynamic_component_test',
      component: DynamicComponentTest
    },
    {
      path: '/sync_test',
      component: SyncTest
    },
    {
      path: '/without_vuex',
      component: WitoutVuexParent
    },
    {
      path: '/with_vuex/input1',
      component: WithVuexInput1,
      beforeEnter (to, from, next) {
        if (from.path !== '/with_vuex/input2') {
          store.commit('userForm/clear')
        }
        next()
      }
    },
    {
      path: '/with_vuex/input2',
      component: WithVuexInput2
    },
    {
      path: '/with_vuex/confirm',
      component: WithVuexConfirm
    },
    {
      path: '/with_vuex/complete',
      component: WithVuexComplete
    }
  ]
})
