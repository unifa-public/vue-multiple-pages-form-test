export interface UserFormState {
  email: string;
  name: string;
  age: number;
}
